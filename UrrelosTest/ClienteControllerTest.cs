﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    public class ClienteControllerTest
    {
        private Mock<ICliente> mockCliente;
        private ClienteController controlCliente;

        [SetUp]
        public void Init()
        {
            mockCliente = new Mock<ICliente>();
            controlCliente = new ClienteController(mockCliente.Object);
        }

        [Test]
        public void ListaClientes()
        {
            mockCliente.Setup(o => o.AllClientes()).Returns( //It.IsAny<Persona>()
                new List<Cliente>()
                {
                    new Cliente {
                    },
                    new Cliente
                    {
                    }
                }
            );


            var resultado = controlCliente.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Cliente>).Count());
        }


        [Test]
        public void NombreClienteRequired()
        {


            controlCliente.Create(
                new Cliente
                {

                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("Nombre"));
        }

        [Test]
        public void ApellidoClienteRequired()
        {

            controlCliente.Create(
                new Cliente
                {

                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("Apellido"));
        }


        [Test]
        public void DireccionClienteRequired()
        {

            controlCliente.Create(
                new Cliente
                {

                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("Direccion"));
        }

        [Test]
        public void EmailClienteRequired()
        {
            controlCliente.Create(
                new Cliente
                {
                    Email = "intkev9@gmailcom"
                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("Email"));
        }


        [Test]
        public void TelefonoCliente9Digitos()
        {

            controlCliente.Create(
                new Cliente
                {
                    Telefono = "97638773"
                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("Telefono"));
        }

        [Test]
        public void RUCDNICliente()
        {

            controlCliente.Create(
                new Cliente
                {
                    DniRuc = "9763873"
                }
            );
            Assert.AreEqual(false, controlCliente.ViewData.ModelState.IsValid);
            Assert.True(controlCliente.ViewData.ModelState.ContainsKey("DniRuc"));
        }

        [Test]
        public void GuardarExitoso()
        {
            controlCliente.Create(new Cliente
            {
                IdCliente = 2,
                Nombre = "Kev",
                Apellido = "Int",
                Direccion = "jr.LosPerdidos",
                DniRuc = "12345678",
                Telefono = "976387738",
                Email = "int.kvn@gmail.com"

            });
            Assert.AreEqual(true, controlCliente.ViewData.ModelState.IsValid);
        }

    }
}
