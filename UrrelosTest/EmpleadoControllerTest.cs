﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    public class EmpleadoControllerTest
    {
        private Mock<IEmpleado> mockEmpleado;
        private EmpleadoController controlEmpleado;
        private Mock<ICargo> mockCargo;
        private CargoController controlCargo;

        [SetUp]
        public void Init()
        {
            mockEmpleado = new Mock<IEmpleado>();
            mockCargo = new Mock<ICargo>();
            controlEmpleado = new EmpleadoController(mockEmpleado.Object, mockCargo.Object);
            controlCargo = new CargoController(mockCargo.Object);
            MetodosProductos.LlamarMetodos(mockEmpleado, mockCargo);
        }


        [Test]
        public void ListaProductos()
        {


            mockEmpleado.Setup(o => o.AllEmpleado()).Returns(new List<Empleado>());
            var view = controlEmpleado.Index() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(view);
            Assert.IsNotNull(view.Model);

        }

        [Test]
        public void NombreEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {

                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Nombre"));
        }

        [Test]
        public void SueldoEmpleadoMayora0Required()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    sueldo = -1
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("sueldo"));
        }


        [Test]
        public void GeneroEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("genero"));
        }

        [Test]
        public void ApellidoEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Apellido"));
        }

        [Test]
        public void DireccionEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Direccion"));
        }

        [Test]
        public void DNIEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    Dni = "1234567"
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Dni"));
        }

        [Test]
        public void TelefonoEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    Telefono = "12345678"
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Telefono"));
        }

        [Test]
        public void EmailEmpleadoRequired()
        {


            controlEmpleado.Create(
                new Empleado
                {
                    Email = "intkev@gmailcom"
                }
            );
            Assert.AreEqual(false, controlEmpleado.ViewData.ModelState.IsValid);
            Assert.True(controlEmpleado.ViewData.ModelState.ContainsKey("Email"));
        }

        [Test]
        public void GuardarExitosoEmpleado()
        {
            mockCargo.Setup(o => o.GetByIdCargo(1)).Returns( //It.IsAny<Persona>()
               new Cargo
               {
                  nombrecargo = "Administrador"
               }
           );
            mockEmpleado.Object.AddEmpleado(new Empleado
            {
              Nombre="Kevin",
              sueldo = 1500,
              genero = "Masculino",
              Apellido = "Into",
              Direccion = "Jr.Los Libertadores",
              Dni = "12345678",
              Telefono = "123456789",
              Email = "intkev@gmail.com"

            });
            Assert.AreEqual(true, controlEmpleado.ViewData.ModelState.IsValid);
        }


    }


    public class MetodosProductos
    {

        public static void LlamarMetodos(Mock<IEmpleado> mockEmpleado, Mock<ICargo> mockCargo)
        {
            mockEmpleado.Setup(o => o.AllCargos()).Returns( //It.IsAny<Persona>()
                new List<Cargo>()
                {

                }
            );

            mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

            //mockCategoria.Setup(o => o.GetSexsos()).Returns( //It.IsAny<Persona>()
            //    new List<categoria>()
            //    {

            //    }
            //);
        }
    }

}
