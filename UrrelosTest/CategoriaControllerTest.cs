﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    class CategoriaControllerTest
    {
        private Mock<ICategorias> mockCategoria;
        private CategoriaController controlCategoria;

        [SetUp]
        public void Init()
        {
            mockCategoria = new Mock<ICategorias>();
            controlCategoria = new CategoriaController(mockCategoria.Object);
        }

        [Test]
        public void ListaCategorias()
        {
            mockCategoria.Setup(o => o.AllCategoria()).Returns( 
                new List<categoria>()
                {
                    new categoria {
                    },
                    new categoria
                    {
                    }
                }
            );
            var resultado = controlCategoria.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<categoria>).Count());

        }

        [Test]
        public void NombreCategoriaRequired()
        {


            controlCategoria.Create(
                new categoria
                {

                }
            );
            Assert.AreEqual(false, controlCategoria.ViewData.ModelState.IsValid);
            Assert.True(controlCategoria.ViewData.ModelState.ContainsKey("Nombre"));
        }

        [Test]
        public void DescripcionCategoriaRequired()
        {


            controlCategoria.Create(
                new categoria
                {

                }
            );
            Assert.AreEqual(false, controlCategoria.ViewData.ModelState.IsValid);
            Assert.True(controlCategoria.ViewData.ModelState.ContainsKey("Descripcion"));
        }


        [Test]
        public void GuardarExitosoCategoria()
        {
          
            mockCategoria.Object.AddCategoria(new categoria
            {
               nombre = "inyeccion",
               Descripcion= "ampo"
               
            });
            Assert.AreEqual(true, controlCategoria.ViewData.ModelState.IsValid);
        }
    }
}
