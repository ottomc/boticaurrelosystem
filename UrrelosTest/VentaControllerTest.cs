﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    public class VentaControllerTest
    {
        private Mock<IVenta> mockVenta;
        private VentaController controlVenta;
        private Mock<IEmpleado> mockEmpleado;
        private EmpleadoController controlEmpleado;
        private Mock<ICargo> mockCargo;
        private CargoController controlCargo;
        private Mock<ICliente> mockCliente;
        private ClienteController controlCliente;
        private Mock<Idetalle> mockDetalle;
        private detalleController controlDetalle;
        private Mock<IProducto> mockProducto;
        private ProductoController controlProducto;
        private Mock<ICategorias> mockCategoria;
        private CategoriaController controlCategoria;

        [SetUp]
        public void Init()
        {
            mockVenta = new Mock<IVenta>();
            mockEmpleado = new Mock<IEmpleado>();
            mockCliente = new Mock<ICliente>();
            mockDetalle = new Mock<Idetalle>();
            mockProducto = new Mock<IProducto>();
            mockCategoria = new Mock<ICategorias>();
            mockCargo = new Mock<ICargo>();
            controlVenta = new VentaController(mockDetalle.Object, mockVenta.Object, mockProducto.Object, mockCliente.Object, mockEmpleado.Object);
            controlCategoria = new CategoriaController(mockCategoria.Object);
            controlEmpleado = new EmpleadoController(mockEmpleado.Object, mockCargo.Object);
            controlCargo = new CargoController( mockCargo.Object);
            controlCliente = new ClienteController(mockCliente.Object);
            controlDetalle = new detalleController();
            controlProducto = new ProductoController(mockProducto.Object, mockCategoria.Object);
            MetodosVentas.LlamarMetodos(mockVenta,mockProducto, mockCategoria, mockCliente, mockDetalle, mockEmpleado, mockCargo);
        }

        [Test]
        public void ListaVentas()
        {


            mockVenta.Setup(o => o.AllVenta()).Returns(new List<Venta>());
            var view = controlProducto.Index() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(view);
            Assert.IsNotNull(view.Model);


           }

        [Test]
        public void ClienteVentaRequired()
        {


            controlVenta.crearventa(
                new Venta
                {

                }
            );
            Assert.AreEqual(false, controlVenta.ViewData.ModelState.IsValid);
            Assert.True(controlVenta.ViewData.ModelState.ContainsKey("IdCliente"));
        }


        [Test]
        public void EmpleadoVentaRequired()
        {


            controlVenta.crearventa(
                new Venta
                {
                    
                }
            );
            Assert.AreEqual(false, controlVenta.ViewData.ModelState.IsValid);
            Assert.True(controlVenta.ViewData.ModelState.ContainsKey("IdEmpleado"));
        }

        [Test]
        public void VentaFechaFutura()
        {

            controlVenta.crearventa(
                new Venta
                {
                    fecha = DateTime.Now.Date.AddDays(1)
                }
            );
            Assert.AreEqual(false, controlVenta.ViewData.ModelState.IsValid);
            Assert.True(controlVenta.ViewData.ModelState.ContainsKey("fecha"));
        }

        [Test]
        public void MontoVentaMayora0Required()
        {


            controlVenta.crearventa(
                new Venta
                {
                    monto = 12
                }
            );
            Assert.AreEqual(false, controlVenta.ViewData.ModelState.IsValid);
            Assert.True(controlVenta.ViewData.ModelState.ContainsKey("monto"));
        }



        [Test]
        public void GuardarExitosoVenta()
        {
            mockCliente.Setup(o => o.GetByIdCliente(1)).Returns( //It.IsAny<Persona>()
               new Cliente
               {
                
               }
           );

            mockEmpleado.Setup(o => o.GetByIdEmpleado(1)).Returns( //It.IsAny<Persona>()
             new Empleado
             {

             }
         );
            mockVenta.Object.AddVenta(new Venta
            {
                fecha= DateTime.Now,
                monto = 20,
                anulado = false
            });
            Assert.AreEqual(true, controlProducto.ViewData.ModelState.IsValid);
        }
    }




   
}

public class MetodosVentas
{



    public static void LlamarMetodos(Mock<IVenta> mockVenta, Mock<IProducto> mockProducto, Mock<ICategorias> mockCategoria, Mock<ICliente> mockCliente, Mock<Idetalle> mockDetalle, Mock<IEmpleado> mockEmpleado, Mock<ICargo> mockCargo)
    {
        //mockProducto.Setup(o => o.GetCategorias()).Returns( //It.IsAny<Persona>()
        //    new List<categoria>()
        //    {

        //    }
        //);

        mockVenta.Setup(o => o.GetClientes()).Returns( //It.IsAny<Persona>()
            new List<Cliente>()
            {

            }
        );

        mockVenta.Setup(o => o.GetEmpleados()).Returns( //It.IsAny<Persona>()
       new List<Empleado>()
       {

       }
   );

        mockCategoria.Setup(o => o.AllCategoria()).Returns(new List<categoria>());

        mockCliente.Setup(o => o.AllClientes()).Returns(new List<Cliente>());

        mockEmpleado.Setup(o => o.AllEmpleado()).Returns(new List<Empleado>());

        mockCargo.Setup(o => o.AllCargo()).Returns(new List<Cargo>());

        mockProducto.Setup(o => o.AllProductos()).Returns(new List<Producto>());
        mockVenta.Setup(o => o.AllVenta()).Returns(new List<Venta>());

        //mockDetalle.Setup(o => o.All()).Returns(new List<Producto>());

        //mockCategoria.Setup(o => o.GetSexsos()).Returns( //It.IsAny<Persona>()
        //    new List<categoria>()
        //    {

        //    }
        //);
    }
}