﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    class ProductoControllerTest
    {
        private Mock<IProducto> mockProducto;
        private ProductoController controlProducto;
        private Mock<ICategorias> mockCategorias;
        private CategoriaController controlCategoria;
        [SetUp]
        public void Init()
        {
            mockProducto = new Mock<IProducto>();
            mockCategorias = new Mock<ICategorias>();
            controlProducto = new ProductoController(mockProducto.Object, mockCategorias.Object);     
            controlCategoria = new CategoriaController(mockCategorias.Object);
            Metodos.LlamarMetodos(mockProducto, mockCategorias);
        }


        [Test]
        public void ListaProductos()
        {


            mockProducto.Setup(o => o.AllProductos()).Returns(new List<Producto>());
            var view = controlProducto.Index() as ViewResult;

            Assert.IsInstanceOf<ViewResult>(view);
            Assert.IsNotNull(view.Model);


            //mockProducto.Setup(o => o.AllProductos()).Returns( //It.IsAny<Persona>()
            //    new List<Producto>()
            //    {
            //        new Producto {

            //        },
            //        new Producto
            //        {
            //        }
            //    }
            //);


            //var resultado = controlProducto.Index() as ViewResult;
            //Assert.AreEqual(2, (resultado.Model as List<Producto>).Count());
        }

        [Test]
        public void NombreProductoRequired()
        {


            controlProducto.Create(
                new Producto
                {

                }
            );
            Assert.AreEqual(false, controlProducto.ViewData.ModelState.IsValid);
            Assert.True(controlProducto.ViewData.ModelState.ContainsKey("Nombre"));
        }

        [Test]
        public void PrecioUnitarioProductoRequired()
        {

            controlProducto.Create(
                new Producto
                {
                    PrecioUnitario = 0
                }
            );
            Assert.AreEqual(false, controlProducto.ViewData.ModelState.IsValid);
            Assert.True(controlProducto.ViewData.ModelState.ContainsKey("PrecioUnitario"));
        }


        [Test]
        public void DescripcionProductoRequired()
        {

            controlProducto.Create(
                new Producto
                {
                    
                }
            );
            Assert.AreEqual(false, controlProducto.ViewData.ModelState.IsValid);
            Assert.True(controlProducto.ViewData.ModelState.ContainsKey("Descripcion"));
        }


        [Test]
        public void StockProductoMayora0Required()
        {

            controlProducto.Create(
                new Producto
                {
                    
                }
            );
            Assert.AreEqual(false, controlProducto.ViewData.ModelState.IsValid);
            Assert.True(controlProducto.ViewData.ModelState.ContainsKey("Stock"));
        }


   
               [Test]
        public void GuardarExitosoConsulta()
        {
            mockCategorias.Setup(o => o.GetByIdCategoria(1)).Returns( //It.IsAny<Persona>()
               new categoria
               {
                   nombre = "Inyeccion",
                   Descripcion = "Fiebre"
               }
           );
            mockProducto.Object.AddProducto(new Producto
            {
               IdProducto = 1,
               Nombre = "Paracetamol",
               Descripcion="Analgesico",
               PrecioUnitario = 5,
               Stock = 10

            });
            Assert.AreEqual(true, controlProducto.ViewData.ModelState.IsValid);
        }
    

    }




    public class Metodos
    {



        public static void LlamarMetodos(Mock<IProducto> mockProducto, Mock<ICategorias> mockCategoria)
        {
            mockProducto.Setup(o => o.GetCategorias()).Returns( //It.IsAny<Persona>()
                new List<categoria>()
                {

                }
            );

           mockCategoria.Setup(o => o.AllCategoria()).Returns(new List<categoria>());

            //mockCategoria.Setup(o => o.GetSexsos()).Returns( //It.IsAny<Persona>()
            //    new List<categoria>()
            //    {

            //    }
            //);
        }
    }
}
