﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UrrelosWeb.Controllers;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosTest
{
    [TestFixture]
    public class CargoControllerTest
    {
        private Mock<ICargo> mockCargo;
        private CargoController controlCargo;

        [SetUp]
        public void Init()
        {
            mockCargo = new Mock<ICargo>();
            controlCargo = new CargoController(mockCargo.Object);
        }


        [Test]
        public void ListaCargos()
        {
            mockCargo.Setup(o => o.AllCargo()).Returns(
                new List<Cargo>()
                {
                    new Cargo {
                    },
                    new Cargo
                    {
                    }
                }
            );
            var resultado = controlCargo.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Cargo>).Count());

        }

        [Test]
        public void NombreCargoRequired()
        {


            controlCargo.Create(
                new Cargo
                {

                }
            );
            Assert.AreEqual(false, controlCargo.ViewData.ModelState.IsValid);
            Assert.True(controlCargo.ViewData.ModelState.ContainsKey("nombrecargo"));
        }
    }
}
