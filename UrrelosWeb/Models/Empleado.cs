﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrrelosWeb.Models
{
    public class Empleado
    {
        [Key]
        [Required]
        public Int32 IdEmpleado { get; set; }
        public string Nombre { get; set; }

        public Decimal sueldo { get; set; }

        public String genero { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Dni { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

        public Cargo cargo { get; set; }
        public Int32 idcargo { get; set; }

    }
}