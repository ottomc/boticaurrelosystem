﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UrrelosWeb.Models
{
    public class categoria
    {
        [Key]
        [Required]
        public Int32 idcategoria { get; set; }
        public string nombre { get; set; }
        public String Descripcion { get; set; }
    }
}