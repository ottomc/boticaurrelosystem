﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrrelosWeb.Models
{
    public class Venta
    {
        [Key]
        [Required]
        public Int32 IdVenta { get; set; }
        public Cliente cliente { get; set; }
        public Int32 IdCliente { get; set; }
        public Empleado empleado { get; set; }
        public Int32 IdEmpleado { get; set; }
        public DateTime? fecha { get; set; }
        public Decimal monto { get; set; }
        public Boolean anulado { get; set; }

        public virtual List<Detalle_Venta> detalleventa { get; set; }
    
    }
}