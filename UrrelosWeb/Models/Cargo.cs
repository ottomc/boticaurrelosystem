﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UrrelosWeb.Models
{
    public class Cargo
    {
        [Key]
        [Required]
        public Int32 Idcargo { get; set; }

        public String nombrecargo{ get; set; }

    }
}