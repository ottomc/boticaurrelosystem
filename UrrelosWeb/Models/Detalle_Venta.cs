﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrrelosWeb.Models
{
    public class Detalle_Venta
    {
        [Key]
        [Required]
        public Int32 IdDetalleVenta { get; set; }
        public Int32 cantidad { get; set; }
        public Decimal preciodetalle { get; set; }
        public Decimal Total { get; set; }

        public Producto producto { get; set; }
        public Int32 IdProducto { get; set; }
        public Venta venta { get; set; }
        public Int32 IdVenta { get; set; }
    }
}