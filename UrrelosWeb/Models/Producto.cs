﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrrelosWeb.Models
{
    public class Producto
    {
        [Key]
        [Required]
        public Int32 IdProducto { get; set; }
        public string Nombre { get; set; }
        public Decimal PrecioUnitario { get; set; }
        public string Descripcion { get; set; }
        public Int32 Stock { get; set; }

        public categoria categoria { get; set; }
        public Int32 idcategoria { get; set; }

    }
}