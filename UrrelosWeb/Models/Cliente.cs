﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UrrelosWeb.Models
{
    public class Cliente
    {
        [Key]
        [Required]
        public Int32 IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string DniRuc { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
    }
}