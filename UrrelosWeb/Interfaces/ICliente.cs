﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
    public interface ICliente
    {
        List<Cliente> AllClientes();

        List<Cliente> ByqueryDate(string criterio);
        void AddClientes(Cliente model);

        Cliente GetByIdCliente(int clienteId);

        void EditarCliente(Cliente model);
        void delete(int id);
        IQueryable<Cliente> GetClientesByQueryAll(string criterio);
    }
}
