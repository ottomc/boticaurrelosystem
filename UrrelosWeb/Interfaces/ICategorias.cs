﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
    public interface ICategorias
    {
        List<categoria> AllCategoria();
        void AddCategoria(categoria model);

        categoria GetByIdCategoria(int categoriaId);

        void Editarcategoria(categoria model);
        void delete(int id);
    }
}
