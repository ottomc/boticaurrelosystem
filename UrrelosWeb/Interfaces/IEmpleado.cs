﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
   public interface IEmpleado
    {
        List<Empleado> AllEmpleado();
        void AddEmpleado(Empleado model);

        Empleado GetByIdEmpleado(int EmpleadoId);

        void EditarEmpleado(Empleado model);
        void delete(int idempleado);
        List<Cargo> AllCargos();
    }
}
