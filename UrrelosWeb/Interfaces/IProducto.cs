﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
    public interface IProducto
    {
        List<Producto> AllProductos();
        void AddProducto(Producto model);

        Producto GetByIdProducto(int clienteId);

        void EditarCliente(Producto model);
        List<categoria> GetCategorias();
    }
}
