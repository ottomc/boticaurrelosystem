﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
   public  interface ICargo
    {
        List<Cargo> AllCargo();
        void AddCargo(Cargo model);

        Cargo GetByIdCargo(int cargoId);

        void EditarCargo(Cargo model);
        void delete(int id);
    }
    
}
