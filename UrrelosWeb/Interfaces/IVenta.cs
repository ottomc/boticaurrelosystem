﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrrelosWeb.Models;

namespace UrrelosWeb.Interfaces
{
    public interface IVenta
    {

        List<Venta> AllVenta();
        List<Cliente> Bycriterio(string criterio);

        void AddVenta(Venta model);

        Venta GetByIdVenta(int VentaId);

        void EditarVenta(Venta model);

        List<Cliente> GetClientes();
        List<Empleado> GetEmpleados();
    }
}
