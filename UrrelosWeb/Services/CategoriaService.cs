﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;


namespace UrrelosWeb.Services
{
    public class CategoriaService:ICategorias
    {
        UrrelosContext db = new UrrelosContext();
        public void AddCategoria(categoria model)
        {
            db.Categorias.Add(model);
            db.SaveChanges();
        }

        public List<categoria> AllCategoria()
        {
            return db.Categorias.ToList();
        }

        public void Editarcategoria(categoria model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public categoria GetByIdCategoria(int categoriaId)
        {
            return db.Categorias.ToList().Find(c => c.idcategoria == categoriaId);
        }

        public void delete(int id)
        {
            var result = db.Categorias.ToList().Find(c => c.idcategoria == id);
            db.Categorias.Remove(result);
            db.SaveChanges();
        }
    }
}