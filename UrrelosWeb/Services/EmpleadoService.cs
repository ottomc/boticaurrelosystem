﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Services
{
    public class EmpleadoService : IEmpleado
    {
        UrrelosContext db = new UrrelosContext();
        public void AddEmpleado(Empleado model)
        {
            db.Empleados.Add(model);
            db.SaveChanges();
        }

        public List<Empleado> AllEmpleado()
        {
            return db.Empleados.Include("Cargo").ToList();
        }

        public void EditarEmpleado(Empleado model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Empleado GetByIdEmpleado(int EmpleadoId)
        {
            return db.Empleados.ToList().Find(c => c.IdEmpleado == EmpleadoId);

        }

        public void delete(int idempleado){

         var result=db.Empleados.ToList().Find(c => c.IdEmpleado == idempleado);
         db.Empleados.Remove(result);
         db.SaveChanges();
        }

        public List<Cargo> AllCargos()
        {
            return db.Cargos.ToList();
        }
    }
}