﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Services
{
    public class ClienteService : ICliente
    {
        UrrelosContext db = new UrrelosContext();
        public void AddClientes(Cliente model)
        {
            db.Clientes.Add(model);
            db.SaveChanges();
        }

        public List<Cliente> AllClientes()
        {
            return db.Clientes.ToList();
        }

        public void EditarCliente(Cliente model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Cliente GetByIdCliente(int clienteId)
        {
            return db.Clientes.ToList().Find(c => c.IdCliente == clienteId);
        }

        public void delete(int id)
        {
             var result=db.Clientes.ToList().Find(c => c.IdCliente == id);
             db.Clientes.Remove(result);
             db.SaveChanges();  
        }

        public List<Cliente> ByqueryDate(string criterio)
        {
            var query = from a in db.Clientes.Include("Venta") select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Apellido.ToUpper().Contains(criterio.ToUpper()));
            }
            return query.ToList();
        }

        public IQueryable<Cliente> GetClientesByQueryAll(string criterio)
        {
            var dbQuery = (from p in db.Clientes select p);

            if (!String.IsNullOrEmpty(criterio))
                dbQuery = dbQuery.Where(o => o.Nombre.Contains(criterio));

            return dbQuery.OrderBy(o => o.Nombre);
        }
    }
}