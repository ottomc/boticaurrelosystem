﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;
namespace UrrelosWeb.Services
{
    public class DetalleService:Idetalle
    {
        UrrelosContext db = new UrrelosContext();
        public List<Detalle_Venta> All(Int32 iddetalle) {

            var query =
            (from c in  db.DetalleVentas.Include("Producto")
             where c.IdVenta == iddetalle
             select c);
            return query.ToList();
        }
    }
}