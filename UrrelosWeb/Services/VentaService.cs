﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Services
{
    public class VentaService : IVenta
    {
        
        UrrelosContext db = new UrrelosContext();
        public void AddVenta(Venta model)
        {
            db.Ventas.Add(model);
            db.SaveChanges();
            DisminuirStock(model);
            
        }

       

        private void DisminuirStock(Venta model)
        {
            var productosIds = model.detalleventa.Select(o => o.IdProducto);
            var productos = db.Productos.Where(o => productosIds.Contains(o.IdProducto));

            foreach (var item in productos)
            {
                item.Stock -= model.detalleventa.First(o => o.IdProducto == item.IdProducto).cantidad;
            }

            db.SaveChanges();
        }

        public List<Venta> AllVenta()
        {
            var query = (from t in db.Ventas.Include("Empleado").Include("Cliente") select t);

            return query.ToList();
        }

        public void EditarVenta(Venta model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Venta GetByIdVenta(int VentaId)
        {
             var result = (from t in db.Ventas.Include("Cliente")
                          where t.IdVenta == VentaId
                          select t);
             return result.FirstOrDefault();

        }

        public List<Cliente> GetClientes()
        {
            return db.Clientes.ToList();
        }

        public List<Empleado> GetEmpleados()
        {
            return db.Empleados.ToList();
        }

        public List<Cliente> Bycriterio(string criterio)
        {
            var query = (from a in db.Clientes.Include("Cliente") where a.Nombre ==criterio select a);
            //if (!string.IsNullOrEmpty(criterio))
            //{
            //    query = query.Where(a => a.Apellido.ToUpper().Contains(criterio.ToUpper()));
            //}
            return query.ToList();
        }
    }
}