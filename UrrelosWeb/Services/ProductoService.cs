﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Services
{
    public class ProductoService : IProducto
    {
        UrrelosContext db = new UrrelosContext();
        public void AddProducto(Producto model)
        {
            db.Productos.Add(model);
            db.SaveChanges();
           
        }

       

        public List<Producto> AllProductos()
        {
            return db.Productos.Include("categoria").ToList();
        }

        public void EditarCliente(Producto model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Producto GetByIdProducto(int productoId)
        {
            return db.Productos.ToList().Find(c => c.IdProducto == productoId);
        }

        public List<categoria> GetCategorias()
        {
            return db.Categorias.ToList();

        }
    }
}