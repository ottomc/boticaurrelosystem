﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Services
{
    public class CargoService : ICargo
    {
        UrrelosContext db = new UrrelosContext();
        public void AddCargo(Cargo model)
        {
            db.Cargos.Add(model);
            db.SaveChanges();
        }

        public List<Cargo> AllCargo()
        {
            return db.Cargos.ToList();
        }

        public void EditarCargo(Cargo model)
        {
            db.Entry(model).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Cargo GetByIdCargo(int cargoId)
        {
            return db.Cargos.ToList().Find(c => c.Idcargo == cargoId);
        }

        public void delete(int id)
        {
            var result = db.Cargos.ToList().Find(c => c.Idcargo == id);
            db.Cargos.Remove(result);
            db.SaveChanges();
        }
    }
}