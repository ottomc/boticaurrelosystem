﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using UrrelosWeb.Models;

namespace UrrelosWeb.Context
{
    public class UrrelosContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Detalle_Venta> DetalleVentas { get; set; }
        public DbSet<Empleado> Empleados { get; set; }

        public DbSet<Producto> Productos { get; set; }

        public DbSet<Venta> Ventas { get; set; }
        public DbSet<Cargo> Cargos { get; set; }
        public DbSet<categoria> Categorias { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}