namespace UrrelosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cargo",
                c => new
                    {
                        Idcargo = c.Int(nullable: false, identity: true),
                        nombrecargo = c.String(),
                    })
                .PrimaryKey(t => t.Idcargo);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        IdCliente = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        Direccion = c.String(),
                        DniRuc = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.IdCliente);
            
            CreateTable(
                "dbo.Detalle_Venta",
                c => new
                    {
                        IdDetalleVenta = c.Int(nullable: false, identity: true),
                        CantidadVendida = c.Int(nullable: false),
                        preciodetalle = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IdProducto = c.Int(nullable: false),
                        IdVenta = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdDetalleVenta)
                .ForeignKey("dbo.Producto", t => t.IdProducto)
                .ForeignKey("dbo.Venta", t => t.IdVenta)
                .Index(t => t.IdProducto)
                .Index(t => t.IdVenta);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descripcion = c.String(),
                        Stock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdProducto);
            
            CreateTable(
                "dbo.Venta",
                c => new
                    {
                        IdVenta = c.Int(nullable: false, identity: true),
                        IdCliente = c.Int(nullable: false),
                        IdEmpleado = c.Int(nullable: false),
                        fecha = c.DateTime(),
                        monto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        anulado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdVenta)
                .ForeignKey("dbo.Cliente", t => t.IdCliente)
                .ForeignKey("dbo.Empleado", t => t.IdEmpleado)
                .Index(t => t.IdCliente)
                .Index(t => t.IdEmpleado);
            
            CreateTable(
                "dbo.Empleado",
                c => new
                    {
                        IdEmpleado = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        sueldo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        genero = c.String(),
                        Apellido = c.String(),
                        Direccion = c.String(),
                        Dni = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                        idcargo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdEmpleado)
                .ForeignKey("dbo.Cargo", t => t.idcargo)
                .Index(t => t.idcargo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Venta", "IdEmpleado", "dbo.Empleado");
            DropForeignKey("dbo.Empleado", "idcargo", "dbo.Cargo");
            DropForeignKey("dbo.Detalle_Venta", "IdVenta", "dbo.Venta");
            DropForeignKey("dbo.Venta", "IdCliente", "dbo.Cliente");
            DropForeignKey("dbo.Detalle_Venta", "IdProducto", "dbo.Producto");
            DropIndex("dbo.Empleado", new[] { "idcargo" });
            DropIndex("dbo.Venta", new[] { "IdEmpleado" });
            DropIndex("dbo.Venta", new[] { "IdCliente" });
            DropIndex("dbo.Detalle_Venta", new[] { "IdVenta" });
            DropIndex("dbo.Detalle_Venta", new[] { "IdProducto" });
            DropTable("dbo.Empleado");
            DropTable("dbo.Venta");
            DropTable("dbo.Producto");
            DropTable("dbo.Detalle_Venta");
            DropTable("dbo.Cliente");
            DropTable("dbo.Cargo");
        }
    }
}
