namespace UrrelosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.categoria",
                c => new
                    {
                        idcategoria = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.idcategoria);
            
            AddColumn("dbo.Producto", "idcategoria", c => c.Int(nullable: false));
            CreateIndex("dbo.Producto", "idcategoria");
            AddForeignKey("dbo.Producto", "idcategoria", "dbo.categoria", "idcategoria");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Producto", "idcategoria", "dbo.categoria");
            DropIndex("dbo.Producto", new[] { "idcategoria" });
            DropColumn("dbo.Producto", "idcategoria");
            DropTable("dbo.categoria");
        }
    }
}
