namespace UrrelosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Detalle_Venta", "cantidad", c => c.Int(nullable: false));
            DropColumn("dbo.Detalle_Venta", "CantidadVendida");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Detalle_Venta", "CantidadVendida", c => c.Int(nullable: false));
            DropColumn("dbo.Detalle_Venta", "cantidad");
        }
    }
}
