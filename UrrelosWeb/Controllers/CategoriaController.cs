﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;
namespace UrrelosWeb.Controllers
{
    public class CategoriaController : Controller
    {
        private UrrelosContext db = new UrrelosContext();
        ICategorias _servicecategorias;

        public CategoriaController(ICategorias Servicecategoria)
        {
            _servicecategorias = Servicecategoria;
        }
        // GET: Categoria

        public ActionResult Index()
        {
            var productos = _servicecategorias.AllCategoria();
            return View(productos);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(categoria model)
        {
            if (model.nombre == null)
            {
                ModelState.AddModelError("Nombre", "Se requiere el Nombre");
            }
            if (model.Descripcion == null)
            {
                ModelState.AddModelError("Descripcion", "Se requiere la Descripcion");
            }

            if (ModelState.IsValid)
            {
                _servicecategorias.AddCategoria(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ViewResult Editar(int id)
        {
            var data = _servicecategorias.GetByIdCategoria(id);
            return View("Edit", data);

        }

        // POST: Vehiculo/Edit/5
        [HttpPost]
        public ActionResult Editar(categoria categoria)
        {
            try
            {// TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    _servicecategorias.Editarcategoria(categoria);
                    return RedirectToAction("Index");
                }
                return View(categoria);
            }
            catch
            {
                return View(categoria);
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            _servicecategorias.delete(id);
            TempData["UpdateSucces"] = "Se Élimino correctamente";
            return RedirectToAction("Index");


        }


    }
}