﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;
using PagedList;

namespace UrrelosWeb.Controllers
{
    public class ClienteController : Controller
    {
        ICliente _ServiceCliente;

        public ClienteController(ICliente ServiceCliente)
        {
            _ServiceCliente = ServiceCliente;
        }

        // GET: Cliente
        public ActionResult Index( )
        {
            //var pageIndex = page ?? 1;
            //var datos = _ServiceCliente.GetClientesByQueryAll(query).ToPagedList(pageIndex, 10);
            //if (!String.IsNullOrEmpty(query))
            //{
            //    datos = _ServiceCliente.GetClientesByQueryAll(query).ToPagedList(pageIndex, 10);
            //}
            //return View(datos);
            var clientes = _ServiceCliente.AllClientes();
            return View(clientes);
        }


        public ActionResult Create()
        {
            return View();
              
        }


        [HttpPost]
        public ActionResult Create(Cliente model)
        {
            if (model.Nombre == null)
            {
                ModelState.AddModelError("Nombre", "Se requiere el Nombre");
            }
            if (model.Apellido == null)
            {
                ModelState.AddModelError("Apellido", "Se requiere el Apellido");
            }

            if (model.Direccion == null)
            {
                ModelState.AddModelError("Direccion", "Se requiere la Direccion");
            }
            if (model.Email != null)
            {
                string email = model.Email;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);

                if (match.Success == false)
                {
                    ModelState.AddModelError("Email", "Email tiene el formato incorrecto");
                }
            }

            if (!string.IsNullOrEmpty(model.Telefono) && model.Telefono.Length != 9)
            {
                ModelState.AddModelError("Telefono", "Telefono tiene que tener 9 digitos");
            }

            if (!string.IsNullOrEmpty(model.DniRuc) && (model.DniRuc.Length != 8 && model.DniRuc.Length != 15))
            {
                ModelState.AddModelError("DniRuc", "El Ruc o DNI tiene el formato incorrecto");


            }




            if (ModelState.IsValid)
            {
                _ServiceCliente.AddClientes(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }


         [HttpGet]

        public ViewResult Editar(int id)
        {
            var data = _ServiceCliente.GetByIdCliente(id);
            return View("Edit", data);

        }

        // POST: Vehiculo/Edit/5
        [HttpPost]
        public ActionResult Edit(Cliente cliente)
        {
            try
            {// TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    _ServiceCliente.EditarCliente(cliente);
                    return RedirectToAction("Index");
                }
                return View(cliente);
            }
            catch
            {
                return View(cliente);
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            _ServiceCliente.delete(id);
            TempData["UpdateSucces"] = "Se Élimino correctamente";
            return RedirectToAction("Index");


        }
    }
}