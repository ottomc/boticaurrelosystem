﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Controllers
{
    public class ProductoController : Controller
    {
        private UrrelosContext db = new UrrelosContext();
        IProducto _serviceProducto;
        ICategorias _servicecate;

        public ProductoController(IProducto ServiceProducto, ICategorias Servicecate)
        {
            _serviceProducto = ServiceProducto;
            _servicecate = Servicecate;
        }

        // GET: Producto
        public ActionResult Index()
        {
            var productos = _serviceProducto.AllProductos();
            return View(productos);
        }

        // GET: Producto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Producto/Create
        public ActionResult Create()
        {

            var categoria = _servicecate.AllCategoria();
            ViewData["idcategoria"] = new SelectList(categoria, "idcategoria", "nombre");
            return View();
        }

     [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Producto model)
        {
            if (model.Nombre == null)
            {
                ModelState.AddModelError("Nombre", "Se requiere el Nombre");
            }
            if (model.PrecioUnitario <= 0)
            {
                ModelState.AddModelError("PrecioUnitario", "El precio debe ser mayor a 0");
            }

            if (model.Descripcion == null)
            {
                ModelState.AddModelError("Descripcion", "Se requiere Descripcion");
            }

            if (model.Stock <= 0)
            {
                ModelState.AddModelError("Stock", "Debe ser mayor a 0");
            }

            if (ModelState.IsValid)
            {

                _serviceProducto.AddProducto(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Producto/Edit/5
        public ActionResult Edit(int? id)
        {
            var categoria = _servicecate.AllCategoria();
            ViewData["idcategoria"] = new SelectList(categoria, "idcategoria", "nombre");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Producto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,Nombre,PrecioUnitario,Descripcion,Stock,idcategoria")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(producto);
        }

        // GET: Producto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productos.Find(id);
            db.Productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
