﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Controllers
{
    public class EmpleadoController : Controller
    {
        IEmpleado _serviceEmpleado;
        ICargo _serviceCargo;
        // GET: Empleado

        public EmpleadoController(IEmpleado serviceEmpleado, ICargo serviceCargo)
        {
            _serviceEmpleado = serviceEmpleado;
            _serviceCargo = serviceCargo;
        }
        public ActionResult Index()
        {
            var empleados = _serviceEmpleado.AllEmpleado();
            return View(empleados);
        }
         [HttpGet]
        public ViewResult Create()
        {
           var cargo= _serviceCargo.AllCargo();
           ViewData["idcargo"] = new SelectList(cargo, "IdCargo", "Nombrecargo");
            
            return View("Create");

        }


        [HttpPost]
        public ActionResult Create(Empleado model)
        {
            if (model.Nombre == null)
            {
                ModelState.AddModelError("Nombre", "Se requiere el Nombre");
            }
            if (model.sueldo < 0)
            {
                ModelState.AddModelError("sueldo", "Sueldo Mayo que 0");
            }
            if (model.genero == null)
            {
                ModelState.AddModelError("genero", "Se requiere el Genero");
            }
            if (model.Apellido == null)
            {
                ModelState.AddModelError("Apellido", "Se requiere el Apellido");
            }

            if (model.Direccion == null)
            {
                ModelState.AddModelError("Direccion", "Se requiere la Direccion");
            }

            if (!string.IsNullOrEmpty(model.Dni) && model.Dni.Length != 8)
            {
                ModelState.AddModelError("Dni", "DNI debe tener 8 Digitos");
            }
           

            if (!string.IsNullOrEmpty(model.Telefono) && model.Telefono.Length != 9)
            {
                ModelState.AddModelError("Telefono", "Telefono tiene que tener 9 digitos");
            }

            if (model.Email != null)
            {
                string email = model.Email;
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);

                if (match.Success == false)
                {
                    ModelState.AddModelError("Email", "Email tiene el formato incorrecto");
                }
            }


            var cargo = _serviceCargo.AllCargo();
            ViewData["idcargo"] = new SelectList(cargo, "IdCargo", "Nombrecargo");

            if (ModelState.IsValid)
            {
                _serviceEmpleado.AddEmpleado(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        [HttpGet]
        public ViewResult Editar(int id)
        {
            var cargo = _serviceCargo.AllCargo();
            ViewData["idcargo"] = new SelectList(cargo, "IdCargo", "Nombrecargo");
            var data =_serviceEmpleado.GetByIdEmpleado(id);
            return View("Edit", data);

        }

        // POST: Vehiculo/Edit/5
        [HttpPost]
        public ActionResult Editar(Empleado empleado)
        {
            try
            {// TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    _serviceEmpleado.EditarEmpleado(empleado);
                    return RedirectToAction("Index");
                }
                return View(empleado);
            }
            catch
            {
                return View("Index");
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            _serviceEmpleado.delete(id);
            TempData["UpdateSucces"] = "Se Élimino correctamente";
            return RedirectToAction("Index");


        }
    }
}