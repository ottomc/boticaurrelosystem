﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Controllers
{
    public class VentaController : Controller
    {
        IVenta _serviceVenta;
        IProducto _producto;
        ICliente _cliente;
        IEmpleado _empleado;
        Idetalle _detalle;
        // GET: Venta

        public VentaController(Idetalle serviceDetalle,IVenta ServiceVenta, IProducto Serviceproducto, ICliente ServiceCliente,
        IEmpleado ServiceEmpleado)
        {
            _serviceVenta = ServiceVenta;
            _producto = Serviceproducto;
            _cliente=ServiceCliente;
             _empleado=ServiceEmpleado;
             _detalle = serviceDetalle;
        }

        public ActionResult Index(int? page, string query = "")
        {
            var pageIndex = page ?? 1;
            var datos = _serviceVenta.AllVenta();
            if (!String.IsNullOrEmpty(query))
            {
                datos = _serviceVenta.AllVenta();
            }
            return View(datos);
        }

        [HttpGet]
        public PartialViewResult addProducto(int id, int index)
        {
            var producto = _producto.GetByIdProducto(id);
            ViewBag.Index = index;
            return PartialView("addProducto", producto);
        }



        [HttpGet]
        public ViewResult Create()
        {
            var lista = _producto.AllProductos().ToList();
            var cliente = _cliente.AllClientes();
            var empleado = _empleado.AllEmpleado();
            ViewData["IdCliente"] = new SelectList(cliente, "IdCliente", "Nombre");
            ViewData["IdEmpleado"] = new SelectList(empleado, "IdEmpleado", "Nombre");
            
            return View("Create", lista);
        }
        [HttpPost]
        public ActionResult crearventa(Venta venta)
        {

            //if (venta.IdCliente != null)
            //{
            //    ModelState.AddModelError("IdCliente", "Se requiere el Cliente");
            //}
            //if (venta.IdEmpleado != null)
            //{
            //    ModelState.AddModelError("IdEmpleado", "Se requiere el Empleado");
            //}

            //if (venta.fecha > DateTime.Now)
            //    ViewData.ModelState.AddModelError("fecha", "La fecha no puede ser mayor a la fecha actual");

            //if (venta.monto < 0)
            //{
            //    ModelState.AddModelError("monto", "Monto debe ser mayor a 0");
            //}

            if (ModelState.IsValid)
            {
                _serviceVenta.AddVenta(venta);
                    return RedirectToAction("Index");
              }
            return View("Create", venta);

        }

        public ViewResult verdetalle(int id)
        {
            dynamic model = new System.Dynamic.ExpandoObject();
            model.venta = _serviceVenta.GetByIdVenta(id);
            model.detalle = _detalle.All(id);

            return View("verdetalle", model);
        }
    }
}