﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrrelosWeb.Context;
using UrrelosWeb.Interfaces;
using UrrelosWeb.Models;

namespace UrrelosWeb.Controllers
{
    public class CargoController : Controller
    {
       private UrrelosContext db = new UrrelosContext();
        ICargo _servicecarg;

        public CargoController(ICargo Servicecargo)
        {
            _servicecarg = Servicecargo;
        }
        // GET: Categoria

        public ActionResult Index()
        {
            var productos = _servicecarg.AllCargo();
            return View(productos);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Cargo model)
        {

            if (model.nombrecargo == null)
            {
                ModelState.AddModelError("nombrecargo", "Se requiere el Nombre");
            }


            if (ModelState.IsValid)
            {
                _servicecarg.AddCargo(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ViewResult Editar(int id)
        {
            var data = _servicecarg.GetByIdCargo(id);
            return View("Edit", data);

        }

        // POST: Vehiculo/Edit/5
        [HttpPost]
        public ActionResult Editar(Cargo cargo)
        {
            try
            {// TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    _servicecarg.EditarCargo(cargo);
                    return RedirectToAction("Index");
                }
                return View(cargo);
            }
            catch
            {
                return View(cargo);
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            _servicecarg.delete(id);
            TempData["UpdateSucces"] = "Se Élimino correctamente";
            return RedirectToAction("Index");


        }
    }
}