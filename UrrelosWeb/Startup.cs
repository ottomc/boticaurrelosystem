﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UrrelosWeb.Startup))]
namespace UrrelosWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
