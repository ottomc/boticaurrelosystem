###INTEGRANTES###
#. Intho Arroyo Kevin
#. Medina Cercado, Miguel
#. Moro Palomino Paul.

#SISTEMA DE VENTAS DE LA BOTICA URRELO SYSTEM #
#Usabilidad
  
 #. ¿El usuario hizo muchas preguntas acerca de como funciona el software?
 #. ¿El usuario se queja por la interfaz gráfica?
 #. ¿El sistema cumple con una guía?

#Eficiencia 
  #. ¿Cuanta memoria RAM consume al desplegar la aplicación web ?
  #. ¿Cuanto tiempo me toma en registrar un producto?
  #. ¿Cuanto tiempo demora el usuario en realizar una venta?
#Fiabilidad
  
   #. ¿El sistema se restablece entre 2 a 5 segundos? 
   #. ¿Cuanta data se perdió en un fallo?
   #. ¿Cuanta data se puede recuperar después de un fallo?
#Funcionalidad
  
   #. ¿El sistema cumple con los requerimientos establecidos por el usuario?
   #. ¿El sistema brinda un resultado aceptable al usuario?
   #. ¿Se puede acoplar el sistema con otro?

#Seguridad
   #. ¿El sistema solo permitirá 3 intentos para para poder ingresar al sistema?
   #. ¿El sistema esta protegido contra accesos no autorizados?

#Madurez 
   #. ¿Cuantas fallas mostro el sistema despues de un mes de despliegue?
   #. ¿Cuanto tiempo me toma en implementar un nuevo requerimiento?
   #. ¿Cuanto afecto el desempeño luego de implementar el nuevo requerimiento?

#Portabilidad
   #. ¿El sistema se puede adaptar a las plataformas Chrome , Firefox ?